FROM python:3.8-slim
COPY . /microblog
RUN apt update
RUN apt install -y python-dev gcc
RUN pip install -r microblog/requirements.txt
WORKDIR /microblog/
EXPOSE 2000
CMD uvicorn microblog.main:app --host 0.0.0.0 --port 2000