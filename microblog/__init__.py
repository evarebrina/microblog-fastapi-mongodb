from pymongo import MongoClient
import os

DB_HOST = os.getenv('DB_HOST', 'localhost')
MONGO_USER = os.getenv('MONGO_USER', 'microblog')
MONGO_PASSWORD = os.getenv('MONGO_PASSWORD', 'microblog')
MONGO_DB = os.getenv('MONGO_DB', 'microblog')
MONGO_URI = os.getenv("MONGO_URI", f"mongodb://{MONGO_USER}:{MONGO_PASSWORD}@{DB_HOST}:27017/{MONGO_DB}")
SECRET_KEY = os.getenv("SECRET_KEY", "super-secret")
client = MongoClient(f"mongodb://{MONGO_USER}:{MONGO_PASSWORD}@{DB_HOST}:27017/{MONGO_DB}")
db = client["microblog"]
