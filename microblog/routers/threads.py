from fastapi import APIRouter, HTTPException, Depends
from bson.objectid import ObjectId
from microblog import db
from microblog.helpers import get_current_user
from pydantic import BaseModel
from microblog.helpers import get_password_hash
from starlette.concurrency import run_in_threadpool
from microblog.helpers import authenticate_user, HTTP_401_UNAUTHORIZED, ACCESS_TOKEN_EXPIRE_MINUTES, timedelta, create_access_token

router = APIRouter()


@router.get("/")
async def index():
    return {"Hello": " world!"}


@router.get("/api/v0.1/threads")
async def list_threads():
    threads = await run_in_threadpool(db.threads.find)
    result = []
    for thread in threads:
        result.append({
            'id': str(thread.get("_id")),
            'body': thread.get("body"),
            'author': str(db.users.find_one({'_id': ObjectId(thread.get('user_id'))}).get("_id", None)) if db.users.find_one({'_id': ObjectId(thread.get('user_id'))}) else None
        })
    return result


class Thread(BaseModel):
    body: str


@router.post("/api/v0.1/threads")
async def new_thread(thread: Thread, user=Depends(get_current_user)):
    await run_in_threadpool(db.threads.insert_one, {"body": thread.body, "user_id": user.get("_id")})
    return {
        'status': "You've created a new thread."
    }


@router.get("/api/v0.1/threads/{thread_id}")
async def get_thread(thread_id: str):
    thread = await run_in_threadpool(db.threads.find_one, {"_id": ObjectId(thread_id)})
    posts_raw = await run_in_threadpool(db.posts.find, {"thread_id": ObjectId(thread_id)})
    posts = []
    for post in posts_raw:
        posts.append({
            'author': str(post.get("user_id")),
            'body': post.get("body"),
            'timestamp': post.get("timestamp"),
        })
    return {
        'author': str(thread.get("user_id")) if thread.get("user_id") else None,
        'body': thread.get("body"),
        'posts': posts,
        'timestamp': thread.get("timestamp"),
    }


class Post(BaseModel):
    body: str


@router.post("/api/v0.1/threads/{thread_id}")
async def new_post(thread_id, post: Post, user=Depends(get_current_user)):
    if await run_in_threadpool(db.threads.find_one, {"_id": ObjectId(thread_id)}):
        await run_in_threadpool(db.posts.insert, {"body": post.body, "thread_id": ObjectId(thread_id), "user_id": ObjectId(user.get("_id"))})
        return {
            'status': f"You've created a new post in thread {thread_id}."
        }
    raise HTTPException(status_code=400)


class SignUpData(BaseModel):
    username: str
    email: str
    password: str


@router.post("/sign_up")
async def new_user(sign_up_data: SignUpData):
    if await run_in_threadpool(db.users.find_one, {"username": sign_up_data.username}):
        raise HTTPException(status_code=400)
    if await run_in_threadpool(db.users.find_one, {"email": sign_up_data.email}):
        raise HTTPException(status_code=400)
    password_hash = get_password_hash(sign_up_data.password)
    user = await run_in_threadpool(db.users.insert_one, {"username": sign_up_data.username, "email": sign_up_data.email, "password_hash": password_hash})
    return {
        'status': 'successfully signed up'
    }


class AuthData(BaseModel):
    username: str
    password: str


@router.post("/token")
async def login_for_access_token(auth_data: AuthData):
    user = await authenticate_user(auth_data.username, auth_data.password)
    if not user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": str(user.get("_id"))}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}
